// One Way Mirror user script
// Sean Herrala

if(!window.location.hash) {

  // User code
  var allEvents = ['change'];

  function masterHandler() {
    var socket = io.connect();
    var event = event || window.event;
    owmHash = { 
      'event': event.type,
      'control': this.type,  
      'id': this.id, 
      'value': this.value, 
      'checked': this.checked,
      'time': Math.round(new Date().getTime() / 100)
    }
    console.log(owmHash);
    socket.emit('owmMsg', owmHash);
  }

  function bindAllEvents(selector, handler) {
    var elements = document.querySelectorAll(selector);
    for(var i=0;i<elements.length;i++) {
      for(var k in allEvents) {
        elements[i].addEventListener(allEvents[k], handler, false);
      }
    }
  }

  bindAllEvents(['input', 'select', 'textarea'], masterHandler);

  document.querySelector('input[type=submit]').addEventListener('click', masterHandler, false);

}

else {

  // Snoop code
  // alert('You are snooping');
  document.body.style.backgroundColor = '#bcbcbc';
  var socket = io.connect();
  var log = '';
  var lastAction;
  $('body').append( $( '<input type="button" value="local log" onclick="alert(log);">' ) );

  socket.on('connect', function() {
    console.log('snoop connected');
    socket.on('owmSnoop', function(owmHash) {
      var controlId = '#' + owmHash.id;
      switch(owmHash.control) {
        case 'radio' :
          $(controlId).attr('checked', owmHash.checked);
          $(controlId).prop('checked', owmHash.checked);
          break;
        case 'checkbox' :
          $(controlId).prop('checked', owmHash.checked);
          break;
        case 'textarea' :
          $(controlId).text(owmHash.value);
          $(controlId).val(owmHash.value);
          break;
        case 'select-one' || 'submit' :
          $(controlId).val(owmHash.value);
          break;
        default :
          $(controlId).attr('value', owmHash.value);
      }
      var logLine = '\n';
      if(lastAction) {
        logLine = (owmHash.time - lastAction) / 10 + ' seconds later \n';
      }
      lastAction = owmHash.time;
      Object.keys(owmHash).forEach(function (key) {
        pair = key + ': ' + owmHash[key] + '\n';
        logLine += pair;
      });
      log += ' \n\n' + logLine;
    });
  });

}
