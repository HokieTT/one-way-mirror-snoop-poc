# One Way Mirror Snooping #

## You must be running the owm.js node.js server (port 3030) for this proof of concept to function. ##

### Why is this cool? ###

* Drop-in: The idea is to include the 3 .js files (jquery, socket.io, and owm_user) to any page and give it snoop-ability
* Real-time and logged for playback
* Playback can be timed or step-by-step
* Compiled statistics of user pauses, corrections, abandonment, etc can be useful for A/B testing and troubleshooting


This proof of concept shows a drop-in script for real-time snooping of a user's actions with form controls. If makes use of jQuery and socket.io JavaScript libraries on the client, and node.js with express, socket.io, and several other modules on the server.  There are 2 .htm files that the node.js server can serve on port 3030.  /user.htm (/public/user.htm in the repo) is the page for both the snooped-on user and snooper.  For testing purposes, if this page is located at [http://localhost:3030/user.htm], you can snoop on it from [http://localhost:3030/user.htm#anyhashstring].  Also, /frame.htm will show a side-by-side view of both snooped-on and snooper.  However, for best effect, use different browsers or even different computers to view the two.  This will show that it's using sockets through the web server instead of merely passing javascript data and variabled within the browser.


Any change/addition on the snooped-on screen of an input or textarea control with a unique ID will automatically mirror to the snooper screen.  Try adding a form control (input, textarea, submit for now) to user.htm, giving is a unique id attribute, and then restart the node.js server and reload.  An event listener will be automatically created and messages sent via web socket to and from the server, snooping on the new control.


The node.js server will receive and emit socket messages to and from any modern browser that can access it (not just local to the machine). Real-time applications could include a sales or customer service rep following a customer having trouble with a form. It also logs each received socket message. This is for a future playback feature (both timed as well as step-through), as well as for statistical analysis which would be particularly helpful with A/B testing to determine the most effective control layout, tab order, wording, etc. It can also be expanded (with additional code) to snoop on other page element interactions, such as a user resizing their browser window.


To advance beyond proof of concept, user sessions -- which are already logged by the server -- would need to be presented to the snooper in a manner making clear the context of what they are seeing.
