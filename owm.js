var http = require('http'),
    express = require('express'),
    logger = require('morgan'),
    winston = require('winston'),
    bodyParser = require('body-parser'),
    path = require('path'),
    io = require('socket.io');

var app = express();
app.set('port', process.env.PORT || 3030);
app.use(logger('dev'));
app.use(bodyParser());
app.use(express.static(path.join(__dirname, 'public')));


var server = http.createServer(app);
io = io.listen(server);

server.listen(app.get('port'), function() {
  console.log('running on port ' + app.get('port'));
  winston.add(winston.transports.File, { filename: 'snoop.log' });
});

io.sockets.on('connection', function(socket) {
  socket.on('owmMsg', function(owmHash) {
    console.log(owmHash);
    io.sockets.emit('owmSnoop', owmHash);
    winston.log('info', {client: socket.id, hash: owmHash});
  });
});
